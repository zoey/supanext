import '../styles/globals.scss'
import 'semantic-ui-css/semantic.min.css'

import Navbar from '../components/Navbar'
import { useSession } from '../lib/hooks'
import { SessionContext } from '../lib/sessionContext'

import type { AppProps } from 'next/app'

const App = ({ Component, pageProps }: AppProps) => {
  const session = useSession()

  return (
    <SessionContext.Provider value={session}>
      <Navbar />
      <Component {...pageProps} />
    </SessionContext.Provider>
  )
}

export default App
