import { useState } from 'react'
import { Input, Button, Container, Header, Modal } from 'semantic-ui-react'

import { supabase } from '../utils/supabaseClient'
import styles from '../styles/Auth.module.scss'

const LoginButton = () => {
  const [open, setOpen] = useState<boolean>(false)
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [confirmationMessage, setConfirmationMessage] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const handleLogin = async () => {
    try {
      setLoading(true)
      const { error } = await supabase.auth.signIn({ email })
      if (error) throw error
      setConfirmationMessage('Check your email for the login link!')
      setErrorMessage('')
    } catch (error) {
      setErrorMessage(error.error_description || error.message)
    } finally {
      setLoading(false)
    }
  }

  const handleClose = () => {
    setConfirmationMessage('')
    setErrorMessage('')
    setOpen(false)
  }

  return (
    <Modal
      onClose={handleClose}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button>Login</Button>}
    >
      <Modal.Content>
        {!confirmationMessage && (
          <Container text>
            <Header as="h3">
              Sign in via magic link with your email below
            </Header>
            <Input
              placeholder="Your email"
              onChange={(_, data) => setEmail(data.value)}
            />
            <p className={styles.error}>{errorMessage}</p>
          </Container>
        )}

        {!!confirmationMessage && (
          <Container text>{confirmationMessage}</Container>
        )}
      </Modal.Content>
      <Modal.Actions>
        {!confirmationMessage && (
          <Button color="black" onClick={handleClose}>
            Cancel
          </Button>
        )}
        <Button
          color="blue"
          onClick={confirmationMessage ? handleClose : handleLogin}
          loading={loading}
        >
          {!!confirmationMessage ? 'Close' : 'Submit'}
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default LoginButton
